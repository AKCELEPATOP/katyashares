﻿using KatyaDich.Interfaces;
using KatyaDich.Models;
using KatyaDich.Models.Fuzzy;
using KatyaDich.Models.Fuzzy.Functions;
using KatyaDich.Services;
using KatyaDich.Services.Indicators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Unity;

namespace KatyaDich
{
    public partial class Form1 : Form
    {
        [Dependency]
        public new IUnityContainer Container { get; set; }

        private readonly object dataLock = new object();

        private readonly RSIService RSIService;

        private readonly IShareParser shareParser;

        private readonly IFinamService finamService;

        private readonly IFuzzyService<ShareDay, RSILing> RSIFuzzyService;

        private IList<ShareModel> shares = new List<ShareModel>();

        private DataTable dataTable;

        private BindingSource shareBinding = new BindingSource();

        private RSIFunction RSIFunction = new RSIFunction();

        private ShareFilter<RSILing> filter;

        public Form1(RSIService RSIService, IShareParser shareParser, IFinamService finamService, IFuzzyService<ShareDay, RSILing> fuzzyService)
        {
            this.RSIService = RSIService;
            this.shareParser = shareParser;
            this.finamService = finamService;
            this.RSIFuzzyService = fuzzyService;
            InitializeComponent();
            dataGridViewShares.Dock = DockStyle.Fill;
        }

        private void Form1_Load(object sender, EventArgs e)
        { 
            SetupComboBox();
            dataGridViewShares.DataSource = shareBinding;

            Task.Run(() => shareParser.GetShares(async (share) =>
             {
                 //TODO при добавлении нескольких индикаторов переделать 
                 share.ShareDays = await finamService.GetShareDays(share);
                 share.ShareDays = RSIService.CalculateForEach(share.ShareDays);
                 RSIService.SetValue(share);

                 lock (dataLock)
                 {
                     var next = RSIFuzzyService.Predict(
                     share.ShareDays,
                     RSIFuzzyService.GetRules(
                         RSIFuzzyService.TranslateToFuzzy(
                             share.ShareDays,
                             RSIFunction
                             )
                         )
                     );
                     share.Predictions.Add(typeof(RSILing), next);

                     shares.Add(share);

                     if (dataTable == null)
                     {
                         InitializeSharesTable();
                     }
                     else
                     {
                         dataGridViewShares.Invoke((Action)(() => dataTable.UpdateDataTable(share)));
                     }
                     UpdateBest();
                 }
             }));

        }

        private void InitializeSharesTable()
        {
            dataTable = shares.ToDataTable();
            dataGridViewShares.Invoke((Action)(() => dataGridViewShares.DataSource = dataTable));
            dataGridViewShares.AutoResizeColumns(
                DataGridViewAutoSizeColumnsMode.Fill);
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!double.TryParse((sender as TextBox).Text + e.KeyChar.ToString(), out double a) && e.KeyChar != 8)
            {
                e.Handled = true;
            }
        }

        private void buttonFilter_Click(object sender, EventArgs e)
        {
            double? from = GetNullableDouble(textBoxFrom);
            double? to = GetNullableDouble(textBoxTo);
            dataGridViewFilter.DataSource = null;
            dataGridViewFilter.DataSource = RSIService.GetRange(shares, from, to).ToDataTable();
        }

        private double? GetNullableDouble(TextBox sender)
        {
            double? result = null;
            if (!string.IsNullOrEmpty(sender.Text))
            {
                if (double.TryParse(sender.Text, out double value))
                    result = value;
            }
            return result;
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void UpdateBest()
        {
            dataGridViewShareBest.Invoke((Action)(() =>
            {
                dataGridViewShareBest.DataSource = null;
                dataGridViewShareBest.DataSource = RSIService.Filter(
                    RSIService.GetBest(shares),
                    filter
                ).ToDataTable();
            }));
        }

        private void UpdateShares()
        {
            dataTable = shares.ToDataTable();
            dataGridViewShares.Invoke((Action)(() => dataGridViewShares.DataSource = dataTable));
        }

        private void лингвистическаяШкалаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var form = new RSI(RSIFunction))
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    lock (dataLock)
                    {
                        RecalculateRSI();
                        UpdateShares();
                        UpdateBest();
                    }
                }
            }
        }

        private void RecalculateRSI()
        {
            foreach (var share in shares)
            {
                var next = RSIFuzzyService.Predict(
                     share.ShareDays,
                     RSIFuzzyService.GetRules(
                         RSIFuzzyService.TranslateToFuzzy(
                             share.ShareDays,
                             RSIFunction
                             )
                         )
                     );
                share.Predictions[typeof(RSILing)] = next;
            }
        }

        private void SetupComboBox()
        {
            var values = Enum.GetValues(typeof(RSILing))
                                        .Cast<RSILing>()
                                        .Select(p => new ComboBoxElement<RSILing> { Name = StringEnum.GetStringValue(p), Value = p })
                                        .ToList();
            values.Insert(0, new ComboBoxElement<RSILing> { Name = "", Value = null });
            comboBox1.DataSource = values;
            comboBox1.DisplayMember = "Name";
            comboBox1.ValueMember = "Value";
            filter = new ShareFilter<RSILing>
            {
                Ling = null
            };
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(filter != null)
            {
                filter.Ling = (comboBox1.SelectedItem as ComboBoxElement<RSILing>).Value ?? null;
            }
            UpdateBest();
        }

        private void dataGridViewShares_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            if (dgv == null)
                return;
            if (dgv.CurrentRow.Selected)
            {
                var externalId = dgv.CurrentRow.Cells[0].Value;
                var share = shares.First(rec => rec.ExternalId.Equals(externalId));
                var form = new RSIDiagram(share);
                form.ShowDialog();
            }
        }
    }

    public class ComboBoxElement<T> where T : struct
    {
        public string Name { get; set; }

        public T? Value { get; set; }
    }
}

﻿using KatyaDich.Interfaces;
using KatyaDich.Models;
using KatyaDich.Models.Fuzzy;
using KatyaDich.Services;
using KatyaDich.Services.Fuzzy;
using KatyaDich.Services.Indicators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

namespace KatyaDich
{
    public static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            var container = GetContainer();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(container.Resolve<Form1>());
        }

        private static IUnityContainer GetContainer()
        {
            IUnityContainer container = new UnityContainer();
            container.RegisterType<RSIService>(new ContainerControlledLifetimeManager());
            container.RegisterType<IFinamService, FinamExportService>(new ContainerControlledLifetimeManager());
            container.RegisterType<IShareParser, AkcciiParserService>(new ContainerControlledLifetimeManager());
            container.RegisterType<IShareParser, AkcciiParserService>(new ContainerControlledLifetimeManager());
            container.RegisterType<IFuzzyService<ShareDay, RSILing>, FuzzyService<ShareDay,RSILing>>(new ContainerControlledLifetimeManager());

            return container;
        }
    }
}

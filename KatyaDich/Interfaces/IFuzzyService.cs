﻿using KatyaDich.Models;
using KatyaDich.Models.Fuzzy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatyaDich.Interfaces
{
    public interface IFuzzyService<T, V>
        where T : ShareDay
        where V : Enum
    {
        IList<T> TranslateToFuzzy(IList<T> list, FuzzyFunction<V> fuzzyFunction);
        IList<Tuple<V, V, V>> GetRules(IList<T> list);

        V GetRule(IList<T> list, params V[] lings);
        
        V Predict(IList<T> list, IList<Tuple<V, V, V>> rules);
    }
}

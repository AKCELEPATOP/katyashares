﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatyaDich.Interfaces
{
    public interface IIndicator<T,V>
    {
        V SetValue(V share);

        IEnumerable<V> GetBest(IEnumerable<V> shares, int count);

        IEnumerable<V> GetRange(IEnumerable<V> shares, double? from, double? to);

        IList<T> CalculateForEach(IList<T> array);
    }
}

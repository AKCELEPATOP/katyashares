﻿using KatyaDich.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatyaDich.Interfaces
{
    public interface IShareParser
    {
        Task GetShares(Func<ShareModel, Task> callback);
    }
}

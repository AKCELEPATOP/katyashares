﻿using KatyaDich.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatyaDich.Interfaces
{
    public interface IFinamService
    {
        Task<ShareDay[]> GetShareDays(ShareModel model);
    }
}

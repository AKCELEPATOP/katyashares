﻿using KatyaDich.Models;
using KatyaDich.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatyaDich
{
    /// <summary>
    /// Методы расширения DataTable
    /// </summary>
    public static class DataTableHelpers
    {
        public static Type indicatorsType = typeof(IDictionary<Indicator, double>);

        public static Type predictionsType = typeof(IDictionary<Type, Enum>);

        public static DataTable ToDataTable<T>(this IEnumerable<T> data)
        {
            PropertyDescriptorCollection properties =
            TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                if (!type.Equals(indicatorsType) && !type.Equals(predictionsType) && !type.Equals(typeof(IList<ShareDay>)))
                {
                    table.Columns.Add(prop.Name, type);
                }
            }
            foreach (T item in data)
            {
                AddRowToTable(table, properties, item);
            }
            return table;
        }
        public static DataTable UpdateDataTable<T>(this DataTable table, T item)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));

            return AddRowToTable(table, properties, item);
        }

        private static DataTable AddRowToTable<T>(DataTable table, PropertyDescriptorCollection properties, T item)
        {
            DataRow row = table.NewRow();
            foreach (PropertyDescriptor prop in properties)
            {
                var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                if (type.Equals(indicatorsType))
                {
                    var value = prop.GetValue(item);
                    if (value != null)
                    {
                        (value as Dictionary<Indicator, double>).Keys.ToList()
                            .ForEach(x =>
                            {
                                var name = x.ToString();
                                if (!table.Columns.Contains(name))
                                {
                                    table.Columns.Add(name, typeof(string));
                                }
                                row[name] = (value as Dictionary<Indicator, double>)[x].ToString("F");
                            });
                    }
                }
                else if (type.Equals(predictionsType))
                {
                    var value = prop.GetValue(item);
                    if (value != null)
                    {
                        (value as Dictionary<Type, Enum>).Keys.ToList()
                            .ForEach(x =>
                            {
                                var name = $"Тренд {StringEnum.GetEnumValue(x)}";
                                if (!table.Columns.Contains(name))
                                {
                                    table.Columns.Add(name, typeof(string));
                                }
                                row[name] = StringEnum.GetStringValue((value as Dictionary<Type, Enum>)[x]);
                            });
                    }
                }
                else if(!type.Equals(typeof(IList<ShareDay>)))
                {
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }
            }
            table.Rows.Add(row);

            return table;
        }
    }
}

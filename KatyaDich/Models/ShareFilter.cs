﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatyaDich.Models
{
    /// <summary>
    /// Набор фильтров для отображения ЦБ
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ShareFilter<T> where T : struct
    {
        public T? Ling { get; set; }
    }
}

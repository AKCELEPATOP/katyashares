﻿using FileHelpers;
using KatyaDich.Models.Fuzzy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatyaDich.Models
{
    /// <summary>
    /// Единичная строка в инфе по ценной бумаге за период
    /// </summary>
    [DelimitedRecord(";")]
    public class ShareDay : BaseFuzzyElement
    {
        [FieldConverter(ConverterKind.Date, "yyyyMMdd")]
        public DateTime Day { get; set; }

        public int Time { get; set; }

        public double Open { get; set; }

        public double High { get; set; }

        public double Low { get; set; }

        public double Close { get; set; }

        public long Vol { get; set; }

        [FieldHidden]
        public override Enum Ling { get; set; }

        [FieldHidden]
        public double RSI { get; set; }
    }
}

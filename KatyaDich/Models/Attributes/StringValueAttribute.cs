﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatyaDich.Models.Attributes
{
    /// <summary>
    /// Аттрибут для обозначения строкового значения перечислений (enum)
    /// </summary>
    public class StringValueAttribute : System.Attribute
    {
        public StringValueAttribute(string value)
        {
            Value = value;
        }

        public string Value { get; }

    }
}

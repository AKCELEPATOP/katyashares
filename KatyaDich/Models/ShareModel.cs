﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatyaDich.Models
{
    /// <summary>
    /// Финальный элемент списка, содержащий всю инфу по бумаге
    /// </summary>
    public class ShareModel
    {
        public string ExternalId { get; set; }
        public string Code { get; set; }

        public string Title { get; set; }

        public IList<ShareDay> ShareDays { get; set; }

        public IDictionary<Indicator, double> Indicators { get; set; } = new Dictionary<Indicator, double>();

        public IDictionary<Type, Enum> Predictions { get; set; } = new Dictionary<Type, Enum>();
    }
}

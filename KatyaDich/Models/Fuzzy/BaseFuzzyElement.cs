﻿using System;

namespace KatyaDich.Models.Fuzzy
{
    /// <summary>
    /// Абстракция для строки статистики цен
    /// </summary>
    public abstract class BaseFuzzyElement
    {
        public abstract Enum Ling { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatyaDich.Models.Fuzzy.Functions
{
    public class RSIFunction : FuzzyFunction<RSILing>
    {
        public RSIFunction()
        {
            functions = new List<Function<RSILing>>()
            {
                new Function<RSILing>()
                {
                    A = 0,
                    B = 0,
                    C = 30,
                    Ling = RSILing.Oversold
                },
                new Function<RSILing>()
                {
                    A = 15,
                    B = 32.5,
                    C = 50,
                    Ling = RSILing.BelowAverage
                },
                new Function<RSILing>()
                {
                    A = 45,
                    B = 50,
                    C = 55,
                    Ling = RSILing.Average
                },
                new Function<RSILing>()
                {
                    A = 50,
                    B = 65,
                    C = 80,
                    Ling = RSILing.AboveAverage
                },
                new Function<RSILing>()
                {
                    A = 70,
                    B = 100,
                    C = 100,
                    Ling = RSILing.Overbought
                }
            };
        }
    }
}

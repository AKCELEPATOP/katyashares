﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatyaDich.Models.Fuzzy
{
    /// <summary>
    /// Базовый элемент временного ряда для метода Сонга
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FuzzyElement<T> where T: Enum
    {
        public T Ling { get; set; }

        public double Value { get; set; }
    }
}

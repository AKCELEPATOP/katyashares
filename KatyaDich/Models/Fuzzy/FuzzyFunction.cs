﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatyaDich.Models.Fuzzy
{
    /// <summary>
    /// Нечеткая шкала
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class FuzzyFunction<T> : ICloneable where T : Enum
    {
        public IList<Function<T>> functions;

        public T AssignValue(double value)
        {
            var func = functions.OrderByDescending(rec => rec.Calculate(value)).FirstOrDefault();
            return func != null ? func.Ling : (T)Enum.ToObject(typeof(T), 0);
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}

﻿using KatyaDich.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatyaDich.Models.Fuzzy
{
    [StringValue("RSI")]
    public enum RSILing
    {
        [StringValue("Перепроданность")] Oversold,
        [StringValue("Ниже среднего")] BelowAverage,
        [StringValue("Среднее")] Average,
        [StringValue("Выше среднего")] AboveAverage,
        [StringValue("Перекупленность")] Overbought,
    }
}

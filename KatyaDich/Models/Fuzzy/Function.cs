﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatyaDich.Models.Fuzzy
{
    /// <summary>
    /// Единичная функция в нечеткой шкале
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Function<T> where T : Enum
    {
        public double A { get; set; }

        public double B { get; set; }

        public double C { get; set; }

        public T Ling { get; set; }

        public double? Calculate(double x)
        {
            double? result = null;

            if (x >= A && x <= C)
            {
                result = CalculateForRange(x);
            }

            return result;
        }

        protected double CalculateForRange(double x)
        {
            double result = 1;
            if (x < B)
            {
                result = GetInterpolate(A, 0, B, 1, x);
            }
            else if (x > B)
            {
                result = GetInterpolate(B, 1, C, 0, x);
            }

            return result;
        }
        
        protected double GetInterpolate(double x1, double y1, double x2, double y2, double x)
        {
            return y2 + (y1 - y2) / (x1 - x2) * (x - x2);
        }
    }
}

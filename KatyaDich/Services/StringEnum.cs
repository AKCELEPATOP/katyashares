﻿using KatyaDich.Models.Attributes;
using System;
using System.Collections;
using System.Reflection;

namespace KatyaDich.Services
{
    /// <summary>
    /// Сервис для получения значения <code>StringValueAttribute</code> посредством рефлексии
    /// </summary>
    public static class StringEnum
    {
        private static Hashtable _stringValues = new Hashtable();

        public static string GetStringValue(Enum value)
        {
            string output = value.ToString();
            Type type = value.GetType();

            if (_stringValues.ContainsKey(value))
            {
                output = (string)_stringValues[value];
            }
            else
            {
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] attrs =
                   fi.GetCustomAttributes(typeof(StringValueAttribute),
                                           false) as StringValueAttribute[];
                if (attrs.Length > 0)
                {
                    _stringValues.Add(value, attrs[0].Value);
                    output = attrs[0].Value;
                }
            }

            return output;
        }

        public static string GetEnumValue(Type type)
        {
            string output = type.ToString();
            if (_stringValues.ContainsKey(type))
            {
                output = (string)_stringValues[type];
            }
            else
            {
                StringValueAttribute stringAttribute =
                    (StringValueAttribute)Attribute.GetCustomAttribute(type, typeof(StringValueAttribute));
                if (stringAttribute != null)
                {
                    _stringValues.Add(type, stringAttribute.Value);
                    output = stringAttribute.Value;
                }
            }

            return output;
        }
    }
}

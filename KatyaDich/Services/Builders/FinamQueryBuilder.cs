﻿using Microsoft.AspNetCore.WebUtilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatyaDich.Services.Builders
{
    /// <summary>
    /// Построение строки запроса для считывания с сайта
    /// </summary>
    class FinamQueryBuilder
    {
        protected IDictionary<string, string> dict;

        public FinamQueryBuilder()
        {
            CreateDictionary();
        }

        protected FinamQueryBuilder CreateDictionary()
        {
            dict = new Dictionary<string, string>
            {
                { "market", "1" },
                { "apply", "0" },
                { "p", "8" },
                { "f", "result" },
                { "e", ".csv" },
                { "dtf", "1" },
                { "tmf", "1" },
                { "MSOR", "1" },
                { "mstime", "on" },
                { "mstimever", "1" },
                { "sep", "3" },
                { "sep2", "1" },
                { "datf", "5" }
            };

            return this;
        }

        public FinamQueryBuilder AppendLastMonth()
        {
            var to = DateTime.Now;
            var from = to.AddMonths(-1);
            dict.Add("df", from.Day.ToString());
            dict.Add("mf", (from.Month - 1).ToString());
            dict.Add("yf", from.Year.ToString());
            dict.Add("from", from.ToString("dd.MM.yyyy"));

            dict.Add("dt", to.Day.ToString());
            dict.Add("mt", (to.Month - 1).ToString());
            dict.Add("yt", to.Year.ToString());
            dict.Add("to", to.ToString("dd.MM.yyyy"));

            return this;
        }

        public FinamQueryBuilder AppendCode(string code)
        {
            dict.Add("code", code);
            dict.Add("cn", code);

            return this;
        }

        public FinamQueryBuilder AppendExternalId(string id)
        {
            dict.Add("em", id);

            return this;
        }

        public override string ToString()
        {
            var query = ConfigurationManager.AppSettings["finamUrl"] + "export.csv";
            return QueryHelpers.AddQueryString(query, dict);
        }
    }
}

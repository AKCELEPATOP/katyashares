﻿using KatyaDich.Interfaces;
using KatyaDich.Models;
using KatyaDich.Models.Fuzzy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatyaDich.Services.Fuzzy
{
    /// <summary>
    /// Метод Сонга
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="V"></typeparam>
    public class FuzzyService<T, V> : IFuzzyService<T,V>
        where T : ShareDay
        where V : Enum
    {
        public IList<T> TranslateToFuzzy(IList<T> list, FuzzyFunction<V> fuzzyFunction)
        {
            foreach (var elem in list)
            {
                elem.Ling = fuzzyFunction.AssignValue(elem.RSI);
            }
            return list;
        }

        public IList<Tuple<V, V, V>> GetRules(IList<T> list)
        {
            var types = Enum.GetValues(typeof(V));
            IList<Tuple<V, V, V>> rules = new List<Tuple<V, V, V>>();
            foreach (V x in types)
            {
                foreach (V y in types)
                {
                    rules.Add(new Tuple<V, V, V>(x, y, GetRule(list, x, y)));
                }
            }

            return rules;
        }

        public V GetRule(IList<T> list, params V[] lings)
        {
            var counters = new Dictionary<V, int>();
            var heap = 0;
            foreach (var item in list)
            {
                if (heap == lings.Length - 1)
                {
                    int current = 0;
                    counters.TryGetValue((V)item.Ling, out current);
                    counters[(V)item.Ling] = current + 1;
                    heap = 0;
                }
                else if (item.Ling.Equals(lings[heap]))
                {
                    heap++;
                }
                else
                {
                    heap = 0;
                }
            }
            V last = (V)Enum.ToObject(typeof(V), 0);
            if (counters.Count > 0)
            {
                last = counters.Aggregate((l, r) => l.Value > r.Value ? l : r).Key;
            }
            else
            {
                last = list.GroupBy(rec => (V)rec.Ling).Aggregate((l, r) => l.Count() > r.Count() ? l : r).Key;
            }

            return last;
        }

        public V Predict(IList<T> list, IList<Tuple<V, V, V>> rules)
        {
            var count = list.Count;
            if (list.Count() < 2)
            {
                throw new Exception("error");
            }
            return rules.Where(
                rec => rec.Item1.Equals(list[count - 2].Ling) &&
                rec.Item2.Equals(list[count - 1].Ling)
                ).FirstOrDefault().Item3;
        }
    }
}

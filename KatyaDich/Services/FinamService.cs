﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FileHelpers;
using KatyaDich.Interfaces;
using KatyaDich.Models;
using KatyaDich.Services.Builders;
using Microsoft.AspNetCore.WebUtilities;

namespace KatyaDich.Services
{
    /// <summary>
    /// Сервис для выгрузки информации по отдельной ЦБ
    /// </summary>
    public class FinamExportService : IFinamService
    {
        private readonly object downloadLock = new object();

        public Task<ShareDay[]> GetShareDays(ShareModel model)
        {
            return Task.Run(() => ParseFile(DownloadFile(model)));
        }

        protected ShareDay[] ParseFile(string filePath)
        {
            var engine = new FileHelperEngine<ShareDay>();
            var records = engine.ReadFile(filePath);
            File.Delete(filePath);
            return records;
        }

        protected string DownloadFile(ShareModel model)
        {
            var path = Path.Combine(
                Path.GetTempPath(),
                $"{model.Code}-{ DateTimeOffset.Now.ToUnixTimeMilliseconds()}.csv"
                );
            lock (downloadLock)
            {
                using (var client = new WebClient())
                {
                    var ss = BuildQueryString(model);
                    client.DownloadFile(BuildQueryString(model), path);
                }
                //задержка, чтобы сервер не возвращал ошибку
                Thread.Sleep(1000);
            }
            return path;
        }
        protected string BuildQueryString(ShareModel model)
        {
            return new FinamQueryBuilder()
                .AppendLastMonth()
                .AppendCode(model.Code)
                .AppendExternalId(model.ExternalId)
                .ToString();
        }
    }
}

﻿using KatyaDich.Interfaces;
using KatyaDich.Models;
using KatyaDich.Models.Fuzzy;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatyaDich.Services.Indicators
{
    /// <summary>
    /// Сервис содержащий все методы по работе с RSI
    /// </summary>
    public class RSIService : IIndicator<ShareDay, ShareModel>
    {
        const Indicator SERVICE_INDICATOR = Indicator.RSI;

        const double MIN_VALUE = 0;

        const double MAX_VALUE = 100;

        public ShareModel SetValue(ShareModel share)
        {
            share.Indicators.Add(SERVICE_INDICATOR, share.ShareDays.Last().RSI);
            return share;
        }

        public IEnumerable<ShareModel> GetBest(IEnumerable<ShareModel> shares, int count = 5)
        {
            var low = int.Parse(ConfigurationManager.AppSettings["rsiLow"]);
            return shares
                .Where(rec => rec.Indicators[SERVICE_INDICATOR] < low)
                .OrderByDescending(rec => rec.Indicators[SERVICE_INDICATOR])
                .Take(count)
                .ToList();
        }

        public IEnumerable<ShareModel> Filter(IEnumerable<ShareModel> shares, ShareFilter<RSILing> filter)
        {
            if(filter == null)
            {
                return shares;
            }
            var result = shares;

            if(filter.Ling.HasValue)
            {
                result = result.Where(rec => (RSILing)rec.Predictions[typeof(RSILing)] == filter.Ling.Value);
            }

            return result;
        }

        public IEnumerable<ShareModel> GetRange(IEnumerable<ShareModel> shares, double? from, double? to)
        {
            return shares
                .Where(rec => rec.Indicators[SERVICE_INDICATOR] >= (from ?? MIN_VALUE)
                    && rec.Indicators[SERVICE_INDICATOR] <= (to ?? MAX_VALUE))
                .OrderByDescending(rec => rec.Indicators[SERVICE_INDICATOR])
                .ToList();
        }

        protected double CalculateRSI(ShareDay[] array)
        {
            return 100 - 100 / (1 + CalculateRS(array));
        }

        protected double CalculateRS(ShareDay[] array)
        {
            double sumUp = 0;
            double sumDown = 0;
            for (var i = 1; i < array.Length; i++)
            {
                var diff = array[i].Close - array[i - 1].Close;
                if (diff > 0)
                {
                    sumUp += diff;
                }
                else
                {
                    sumDown -= diff;
                }
            }
            return sumUp / sumDown;
        }

        public IList<ShareDay> CalculateForEach(IList<ShareDay> array)
        {
            double sumUp = 0;
            double sumDown = 0;
            for (var i = 1; i < array.Count; i++)
            {
                var diff = array[i].Close - array[i - 1].Close;
                if (diff > 0)
                {
                    sumUp += diff;
                }
                else
                {
                    sumDown -= diff;
                }
                array[i].RSI = 100 - 100 / (1 + sumUp / sumDown);
            }
            return array;
        }
    }
}

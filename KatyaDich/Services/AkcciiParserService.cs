﻿using KatyaDich.Interfaces;
using KatyaDich.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace KatyaDich.Services
{
    /// <summary>
    /// Сервис парсинга с сайта всех ценных бумаг (акций)
    /// </summary>
    public class AkcciiParserService : IShareParser
    {
        private readonly Regex share = new Regex(@"Finam.IssuerProfile.Main.issue = .*?""id"": (.*?),.*?""code"": ""(.*?)"".*?""title"": ""(.*?)""", RegexOptions.IgnoreCase);

        protected List<string> GetLinks()
        {
            string htmlPage = string.Empty;
            using (WebClient wc = new WebClient())
            {
                htmlPage = wc.DownloadString(ConfigurationManager.AppSettings["ichartsUrl"]);
            }
            string moex = ConfigurationManager.AppSettings["moex"];
            Regex moexTags = new Regex($@"""{moex}/[a-z0-9\-]+?""", RegexOptions.IgnoreCase);
            return moexTags.Matches(htmlPage)
                .Cast<Match>()
                .Distinct()
                //.Take(3)
                .Select(m => m.Value.Replace("\"", ""))
                .ToList();
        }

        public async Task GetShares(Func<ShareModel, Task> callback)
        {
            await Task.WhenAll((GetLinks()).Select(rec => GetShare(rec, callback)));
        }

        protected Task GetShare(string uri, Func<ShareModel, Task> callback)
        {
            string htmlPage = string.Empty;
            try
            {
                using (WebClient wc = new WebClient())
                {
                    htmlPage = wc.DownloadString($"https://www.finam.ru/profile/{uri}/export/");
                }
            }
            catch(Exception)
            {
                return Task.CompletedTask;
            }
            var res = share.Match(htmlPage);
            if (!res.Success)
            {
                return Task.CompletedTask;
            }
            var model = new ShareModel
            {
                ExternalId = res.Groups[1].Value,
                Code = res.Groups[2].Value,
                Title = res.Groups[3].Value
            };
            callback(model);
            return Task.CompletedTask;
        }
    }
}

﻿using KatyaDich.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace KatyaDich
{
    public partial class RSIDiagram : Form
    {
        private readonly ShareModel share;
        public RSIDiagram(ShareModel share)
        {
            this.share = share;
            InitializeComponent();
        }

        private void SetupChart()
        {
            chart.ChartAreas.Add(new ChartArea("Default"));
            //chart.ChartAreas["Default"].AxisX.LabelStyle.Format = "{0:0}";
            chart.ChartAreas["Default"].AxisY.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
            chart.ChartAreas["Default"].AxisX.MajorGrid.LineDashStyle = ChartDashStyle.Dash;
            chart.Series.Clear();
        }

        private void RSIDiagram_Load(object sender, EventArgs e)
        {
            SetupChart();
            var name = share.Title;
            chart.Series.Add(new Series(name));
            chart.Series[name].ChartArea = "Default";
            chart.Series[name].ChartType = SeriesChartType.Line;
            chart.Series[name].XValueType = ChartValueType.Date;
            foreach (var day in share.ShareDays)
            {
                chart.Series[name].Points.AddXY(day.Day, day.RSI);
            }
        }
    }
}

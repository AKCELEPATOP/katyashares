﻿using KatyaDich.Models.Fuzzy;
using KatyaDich.Models.Fuzzy.Functions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatyaDich
{
    public partial class RSI : Form
    {
        public RSIFunction RSIFunction { get; set; }

        public RSI(RSIFunction RSIFunction)
        {
            this.RSIFunction = RSIFunction;
            InitializeComponent();

            textBoxOversold.Text = FormatFunction(this.RSIFunction.functions[0]);
            textBoxBelowAverage.Text = FormatFunction(this.RSIFunction.functions[1]);
            textBoxAverage.Text = FormatFunction(this.RSIFunction.functions[2]);
            textBoxAboveAverage.Text = FormatFunction(this.RSIFunction.functions[3]);
            textBoxOverbought.Text = FormatFunction(this.RSIFunction.functions[4]);
            chart1.Series.Clear();
            for (int i = 0; i < this.RSIFunction.functions.Count; i++)
            {
       
                chart1.Series.Add(Convert.ToString(this.RSIFunction.functions[i].Ling));
                chart1.Series[i].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line; 
                chart1.Series[i].Points.AddXY(this.RSIFunction.functions[i].A, 0);
                chart1.Series[i].Points.AddXY(this.RSIFunction.functions[i].B, 1);
                chart1.Series[i].Points.AddXY(this.RSIFunction.functions[i].C, 0);
            }
        }

        private void textBoxOversold_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString() == "\b")
            {
                return;
            }
            var result = (sender as TextBox).Text + e.KeyChar.ToString();
            if (!Regex.IsMatch(result, @"^([+-]?\d+?\.?\d*?)( |( [+-]?\d+?\.?\d*?))?( |( [+-]?\d+?\.?\d*?))?$"))
            {
                e.Handled = true;
            }
        }

        private void textBoxOversold_Validating(object sender, CancelEventArgs e)
        {
            
        }

        private string FormatFunction(Function<RSILing> function)
        {
            return $"{function.A} {function.B} {function.C}";
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (!IsValid())
            {
                return;
            }
            DialogResult = DialogResult.OK;
            FillFunction(textBoxOversold, RSIFunction.functions[0]);
            FillFunction(textBoxBelowAverage, RSIFunction.functions[1]);
            FillFunction(textBoxAverage, RSIFunction.functions[2]);
            FillFunction(textBoxAboveAverage, RSIFunction.functions[3]);
            FillFunction(textBoxOverbought, RSIFunction.functions[4]);
            Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void textBoxOversold_Validated(object sender, EventArgs e)
        {
            errorProvider1.SetError(sender as Control, string.Empty);
            if (!Regex.IsMatch((sender as TextBox).Text, @"^([+-]?\d+?(\.\d+?)?) ([+-]?\d+?(\.\d+?)?) ([+-]?\d+?(\.\d+?)?)$"))
            {
                errorProvider1.SetError(sender as Control, "Неверный формат");
            }
        }

        private bool IsValid()
        {
            return validateTextBox(textBoxOversold) &&
                validateTextBox(textBoxBelowAverage) &&
                validateTextBox(textBoxAverage) &&
                validateTextBox(textBoxAboveAverage) &&
                validateTextBox(textBoxOverbought);
        }

        private bool validateTextBox(TextBox textBox)
        {
            return errorProvider1.GetError(textBox).Length == 0;
        }

        private void FillFunction(TextBox textBox, Function<RSILing> function)
        {
            var result = Regex.Matches(textBox.Text, @"[-+0-9,]+");
            function.A = double.Parse(result[0].Value);
            function.B = double.Parse(result[1].Value);
            function.C = double.Parse(result[2].Value);
          
        }
    }
}
